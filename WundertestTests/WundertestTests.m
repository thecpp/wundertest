//
//  WundertestTests.m
//  WundertestTests
//
//  Created by Victor Schepanovsky on 03.11.13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "WTDBFacade.h"

@interface WundertestTests : XCTestCase

@end

@implementation WundertestTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    NSLog(@"Try to add and remove object from database");
    int count = 0, count1 = 0, count2 = 0;
    count = [[WTDBFacade sharedInstance] objectsCount];
    NSLog(@"Count : %u", count);
    [[WTDBFacade sharedInstance] addObjectWithTitle:@"someTitle"];
    count1 = [[WTDBFacade sharedInstance] objectsCount];
    NSLog(@"Count : %u, %@", count1, (count1 == count + 1) ? @"OK" :@"Failed");
    [[WTDBFacade sharedInstance] deleteObjectAtIndex:0];
    count2 = [[WTDBFacade sharedInstance] objectsCount];
    NSLog(@"Count : %u, %@", count2, (count2 == count1 - 1) ? @"OK" :@"Failed");
}

@end
