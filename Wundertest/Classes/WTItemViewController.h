//
//  WTItemViewController.h
//  Wundertest
//
//  Created by Victor Schepanovsky on 04.11.13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WTEntity.h"

@interface WTItemViewController : UIViewController

@property (nonatomic, strong) WTEntity *entity;

@end
