//
//  WTRootViewController.m
//  Wundertest
//
//  Created by Victor Schepanovsky on 03.11.13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import "WTRootViewController.h"
#import "WTDBFacade.h"
#import "WTEntity.h"
#import "WTItemCell.h"
#import "WTItemViewController.h"

@interface WTRootViewController ()
@property (nonatomic, strong) UIButton *addButton;
@end

@implementation WTRootViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.title = NSLocalizedString(@"Wundertest", @"");
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createNewItem)];
    
    [self switchEdit:NO];
}

- (void) viewWillAppear:(BOOL)animated
{
    //reload after creating new item
    [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationAutomatic];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - edit button action

- (void) switchEdit:(BOOL)enableEdit
{
    self.navigationItem.rightBarButtonItem = (!enableEdit) ? [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Edit", @"") style:UIBarButtonItemStyleBordered target:self action:@selector(startEdit:)] : [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(finishEdit:)];
    self.tableView.editing = enableEdit;
}

- (void)startEdit:(id)sender
{
    [self switchEdit:YES];
}

- (void)finishEdit:(id)sender
{
    [self switchEdit:NO];
}

#pragma mark - TableView data source methods

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [WTDBFacade sharedInstance].objectsCount;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseId = @"reuseId";
    WTItemCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
    if (!cell)
    {
        cell = [[WTItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
    }
    
    WTEntity *entity = [[WTDBFacade sharedInstance] objectAtIndex:indexPath.row];
    cell.entity = entity;
    return cell;
}

#pragma mark - TableView delegate methods

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [[WTDBFacade sharedInstance] deleteObjectAtIndex:indexPath.row];
        [self.tableView beginUpdates];
        [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [self.tableView endUpdates];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
    {
        //none
    }
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath
{
    [[WTDBFacade sharedInstance] rearrangeItemAtIndex:sourceIndexPath.row toIndex:destinationIndexPath.row];
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (!tableView.editing)
    {
        WTItemViewController *itemViewController = [WTItemViewController new];
        itemViewController.entity = [[WTDBFacade sharedInstance] objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:itemViewController animated:YES];
    }
}

#pragma mark - create new item action

- (void) createNewItem
{
    WTItemViewController *itemViewController = [WTItemViewController new];
    [self.navigationController pushViewController:itemViewController animated:YES];
}

@end
