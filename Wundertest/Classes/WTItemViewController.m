//
//  WTItemViewController.m
//  Wundertest
//
//  Created by Victor Schepanovsky on 04.11.13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import "WTItemViewController.h"
#import "WTDBFacade.h"

@interface WTItemViewController ()

@property (nonatomic, strong) UITextField *textField;

@end

@implementation WTItemViewController

#pragma mark - Setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(3, 3, self.view.frame.size.width-6, 30)];
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.placeholder = NSLocalizedString(@"Title", @"");
    self.textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.textField];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", @"") style:UIBarButtonItemStyleDone target:self action:@selector(done)];
}

- (void) viewWillAppear:(BOOL)animated
{
    if (self.entity)
    {
        self.textField.text = self.entity.title;
    }
}

#pragma mark - Save/Update on Done

- (void) done
{
    if (!self.entity)
    {
        self.entity = [[WTDBFacade sharedInstance] addObjectWithTitle:self.textField.text];
    }
    else
    {
        self.entity.title = self.textField.text;
        [[WTDBFacade sharedInstance] saveContext];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
