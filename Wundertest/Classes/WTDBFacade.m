//
//  WTDBFacade.m
//  Wundertest
//
//  Created by Victor Schepanovsky on 03.11.13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import "WTDBFacade.h"
#import "WTEntity.h"

@interface WTDBFacade () <NSFetchedResultsControllerDelegate>

// atomic for multi-thread access
@property (readonly, strong, atomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (readonly, strong, atomic) NSFetchedResultsController *fetchedResultsController;

- (NSURL *)applicationDocumentsDirectory;

@end

@implementation WTDBFacade
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize fetchedResultsController = _fetchedResultsController;

#pragma mark - shared methods

+ (WTDBFacade *)sharedInstance
{
    static dispatch_once_t pred;
    static WTDBFacade *sharedInstance = nil;
    dispatch_once(&pred, ^{
        sharedInstance = [[WTDBFacade alloc] init];
    });
    return sharedInstance;
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil)
    {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil)
    {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Wundertest" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil)
    {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Wundertest.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }

    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


#pragma mark - Fetch Data

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController == nil)
    {
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"WTEntity" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:NO];
        NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
        
        [fetchRequest setSortDescriptors:sortDescriptors];
        
        NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Root"];
        aFetchedResultsController.delegate = self;
        _fetchedResultsController = aFetchedResultsController;
    }

    return _fetchedResultsController;
}

#pragma mark - Manage Objects

- (WTEntity *) objectAtIndex:(NSUInteger)index
{
    return [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
}

- (NSUInteger) objectsCount
{
    NSError *error = nil;
    [self.fetchedResultsController performFetch:&error];
    if (error)
    {
        NSLog(@"Fetch failed : %@", error);
    }
    return [self.fetchedResultsController.fetchedObjects count];
}

- (WTEntity *) addObjectWithTitle:(NSString *)title
{
    WTEntity *entity = nil;
    @try
    {
        @synchronized(self.managedObjectContext)
        {
            entity = [NSEntityDescription insertNewObjectForEntityForName:@"WTEntity" inManagedObjectContext:self.managedObjectContext];
            entity.created = [NSDate date];
            entity.title = title;
            entity.order = [NSNumber numberWithInteger:self.fetchedResultsController.fetchedObjects.count];
            [self saveContext];
        }
    }
    @catch (NSException *e)
    {
        NSLog(@"Failure in create name=%@ reason=%@", e.name, e.reason);
    }
    return entity;
}

- (void) deleteObjectAtIndex:(NSUInteger)index
{
    @try {
        @synchronized(self.fetchedResultsController)
        {
            for (int i = 0; i < index; i++)
            {
                WTEntity *entity = [self objectAtIndex:i];
                entity.order = [NSNumber numberWithInt:[entity.order intValue] - 1];
            }
            
            WTEntity * entity = [self.fetchedResultsController objectAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            [self.managedObjectContext deleteObject:entity];
            [self saveContext];
        }
    }
    @catch (NSException * e)
    {
        NSLog(@"Failure in delete name=%@ reason=%@", e.name, e.reason);
    }
}

- (void) rearrangeItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex
{
    if (fromIndex != toIndex)
    {
        @synchronized(self.fetchedResultsController)
        {
            int count = self.fetchedResultsController.fetchedObjects.count;
            int sign = (int)(toIndex - fromIndex) / abs(toIndex - fromIndex);
            for (int i = fromIndex; i != toIndex; i += sign)
            {
                WTEntity *entity = [self objectAtIndex:i+sign];
                entity.order = [NSNumber numberWithInt:count - 1 - i];
            }
            WTEntity *entity = [self objectAtIndex:fromIndex];
            entity.order = [NSNumber numberWithInt:count - toIndex -1];
            [self saveContext];
        }
    }
}


@end
