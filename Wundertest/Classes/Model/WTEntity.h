//
//  Entity.h
//  Wundertest
//
//  Created by Victor Schepanovsky on 11/4/13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WTEntity : NSManagedObject

@property (nonatomic, retain) NSDate * created;
@property (nonatomic, retain) NSDate * dueTime;
@property (nonatomic, retain) NSNumber * favourite;
@property (nonatomic, retain) NSNumber * finished;
@property (nonatomic, retain) NSString * notes;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSDate * remindTime;
@property (nonatomic, retain) NSString * title;

@end
