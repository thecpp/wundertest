//
//  Entity.m
//  Wundertest
//
//  Created by Victor Schepanovsky on 11/4/13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import "WTEntity.h"


@implementation WTEntity

@dynamic created;
@dynamic dueTime;
@dynamic favourite;
@dynamic finished;
@dynamic notes;
@dynamic order;
@dynamic remindTime;
@dynamic title;

@end
