//
//  WTItemCell.h
//  Wundertest
//
//  Created by Victor Schepanovsky on 11/4/13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WTEntity.h"

@interface WTItemCell : UITableViewCell

@property (nonatomic, assign) BOOL checked;
@property (nonatomic, weak) WTEntity *entity;

@end
