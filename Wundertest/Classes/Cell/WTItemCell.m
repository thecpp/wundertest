//
//  WTItemCell.m
//  Wundertest
//
//  Created by Victor Schepanovsky on 11/4/13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import "WTItemCell.h"
#import "WTDBFacade.h"

#define kCheckButtonSize 24

@interface WTItemCell ()

@property (nonatomic, strong) UIButton *checkButton;

@end

@implementation WTItemCell

#pragma mark - Custom cell setup

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        float step = (self.contentView.frame.size.height - kCheckButtonSize) / 2;
        self.checkButton = [[UIButton alloc] initWithFrame:CGRectMake(step, step, kCheckButtonSize, kCheckButtonSize)];
        [self.checkButton addTarget:self action:@selector(check) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.checkButton];
        self.checked = NO;
    }
    return self;
}

- (void) layoutSubviews
{
    [super layoutSubviews];
    float step = (self.contentView.frame.size.height - kCheckButtonSize) / 2;
    self.textLabel.frame = CGRectMake(2*step + kCheckButtonSize, self.textLabel.frame.origin.y, self.contentView.frame.size.width - (3*step + kCheckButtonSize), self.textLabel.frame.size.height);
}

#pragma mark - check on tap

- (void) check
{
    self.checked = !self.checked;
    self.entity.finished = [NSNumber numberWithBool:self.checked];
    [[WTDBFacade sharedInstance] saveContext];
}

- (void) setChecked:(BOOL)checked
{
    _checked = checked;
    UIImage *image = [UIImage imageNamed:(checked ? @"checked" : @"unchecked")];
    [self.checkButton setBackgroundImage:image forState:UIControlStateNormal];
}

#pragma mark - Setup cell content from entity 

- (void) setEntity:(WTEntity *)entity
{
    _entity = entity;
    self.textLabel.text = self.entity.title;
    self.checked = [self.entity.finished boolValue];
}

@end
