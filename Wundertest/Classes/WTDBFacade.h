//
//  WTDBFacade.h
//  Wundertest
//
//  Created by Victor Schepanovsky on 03.11.13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WTEntity.h"

@interface WTDBFacade : NSObject 

+ (WTDBFacade *) sharedInstance;

- (NSUInteger) objectsCount;
- (WTEntity *) objectAtIndex:(NSUInteger)index;
- (WTEntity *) addObjectWithTitle:(NSString *)title;
- (void) deleteObjectAtIndex:(NSUInteger)index;

- (void) rearrangeItemAtIndex:(NSUInteger)fromIndex toIndex:(NSUInteger)toIndex;

- (void)saveContext;

@end
