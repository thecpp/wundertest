//
//  WTAppDelegate.h
//  Wundertest
//
//  Created by Victor Schepanovsky on 03.11.13.
//  Copyright (c) 2013 Victor Schepanovsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
